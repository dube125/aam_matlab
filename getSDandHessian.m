function [H, Hinv, SD] = getSDandHessian(s, s0, t, avgfaceimg)

avgfacemarker = getStandardfacemarker(s0);

RS = size(s,2) + 4;

s_star = [s0, [s0(51:100); s0(1:50)], [ones(50, 1);zeros(50,1)] [zeros(50,1); ones(50,1)]];

s = [s s_star];

[fvidx, ~] = getFaceVertexIndices();

[destlm, ~, ~] = getDestLookupMap(avgfacemarker, fvidx);

[m, n, ~] = size(destlm);

dw_dx = zeros(m, n, 50);

for i = 1:50
    [Tidx ~] = find(fvidx == i);
    for j = 1:length(Tidx)
        triangle_indices =  fvidx(Tidx(j),:);
        other_vectices = triangle_indices(triangle_indices ~= i);
        p0 = zeros(3,2);
        p0(1,:) = avgfacemarker(i,:);
        p0(2,:) = avgfacemarker(other_vectices(1),:);
        p0(3,:) = avgfacemarker(other_vectices(2),:);
       
        a_denu = [p0(3,2)-p0(1,2) -p0(3,1)+p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
        b_denu = [-p0(2,2)+p0(1,2) p0(2,1)-p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
   
        [pixel_y pixel_x] = find(destlm == Tidx(j));
        for k = 1:length(pixel_x)
            dw_dx(pixel_y(k), pixel_x(k), i) = 1 - ([pixel_x(k)+0.5 pixel_y(k)+0.5] - p0(1,:))*(a_denu + b_denu);
        end
    end
end


dw_dp_x = zeros(m, n, RS);
dw_dp_y = zeros(m, n, RS);
for i = 1:50
    for j = 1:RS
        dw_dp_x(:,:,j) = dw_dp_x(:,:,j) + s(i,j)*dw_dx(:,:,i);
        dw_dp_y(:,:,j) = dw_dp_y(:,:,j) + s(i+50,j)*dw_dx(:,:,i);  
    end
end

 

[T0imgdx, T0imgdy] = gradient(avgfaceimg);


star_term = zeros(m, n, 3, RS);

for i = 1:RS
    for j = 1:3
        star_term(:,:,j,i) = T0imgdx(:,:,j) .* dw_dp_x(:,:,i) + T0imgdy(:,:,j) .* dw_dp_y(:,:,i);
    end
end

SD = zeros(m, n, 3, RS);

RT = size(t,2);

for i = 1:RS
    SD(:,:,:,i) = star_term(:,:,:,i);
%     for j = 1:RT
%         SD(:, :, :, i) = SD(:, :, :, i) - reshape((reshape(SD(:,:,:,i), m*n*3,1)'*t(:,j))*t(:,j), m,n,3,1);
%     end
end


H = zeros(RS, RS);
SD_at_pixel = zeros(3, RS);
for x = 1:n
    for y = 1:m
        SD_at_pixel(:,:) = SD(y,x,:,:);
        H = H + SD_at_pixel' * SD_at_pixel;
    end
end

Hinv = inv(H);


if nargout == 0

    figure
    for i = 1:50
        imshow(dw_dx(:,:,i));
        pause;
    end

    close all
    figure
    for i = 1:RS
        subplot(2,1,1);
        imshow(dw_dp_x(:,:,i), [-0.5,0.5]);
        subplot(2,1,2);
        imshow(dw_dp_y(:,:,i), [-0.5,0.5]);
        pause;
    end

    figure;

    subplot(2,1,1);
    imshow(T0imgdx(:,:,1), [-0.1, 0.1]);
    subplot(2,1,2);
    imshow(T0imgdy(:,:,1), [-0.1, 0.1]);
end


end