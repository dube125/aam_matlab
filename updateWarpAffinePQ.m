function new_pq = updateWarpAffinePQ(T0, s0, s, srcimage, pq, SD, Hinv)

RS = size(pq,1)-4;

p = pq(1:RS);
q = pq(RS+1:RS+4);

ws = s*p + s0;

s_star = zeros(100,4);

s_star(:,1) = s0(:);
s_star(1:50,2) = -s0(51:100);
s_star(51:100,2) = s0(1:50);
s_star(1:50,3) = 1;
s_star(51:100,4) = 1;

s_target = ws + s_star*q;
s_target_marker = getStandardfacemarker(s_target);
s0_marker = getStandardfacemarker(s0);

[residue, Idiff, warpedImage] = getResidue(T0, s0, s, srcimage, pq);

[m,n,~] = size(Idiff);

SD_transpose_Idiff = zeros(RS+4,1);
Idiff_at_pixel = zeros(3,1);
for x = 1:n
    for y = 1:m
        SD_at_pixel(:,:) = SD(y,x,:,:);
        Idiff_at_pixel(:,1) = Idiff(y,x,:);
        SD_transpose_Idiff(:,:) = SD_transpose_Idiff(:,:) + SD_at_pixel'*Idiff_at_pixel;
    end
end

dpq = Hinv*SD_transpose_Idiff/256;


dp = dpq(1:RS);
dq = dpq(RS+1:RS+4);

w_s0_dp = -s*dp+s0;
s_star_dp = zeros(100,4);

s_star_dp(:,1) = w_s0_dp(:);
s_star_dp(1:50,2) = -w_s0_dp(51:100);
s_star_dp(51:100,2) = w_s0_dp(1:50);
s_star_dp(1:50,3) = 1;
s_star_dp(51:100,4) = 1;

nw_s0_dp_dq = getTranslate(s0, s, -dp, -dq);


d_nw_s0_dp_dq= -s_star_dp*dq + w_s0_dp-s0;


ds = getIncrementalWarp(s0, d_nw_s0_dp_dq, s_target);

[fvidx bgidx] = getFaceVertexIndices();

figure(1)
subplot(1,2,1);
showmarkings(srcimage, s_target_marker);
quiver(s_target_marker(:,1), s_target_marker(:,2), ds(1:50), ds(51:100));
trimesh(fvidx, s_target_marker(:,1), s_target_marker(:,2));

subplot(1,2,2);
showmarkings(warpedImage, s0_marker);
quiver(s0_marker(:,1), s0_marker(:,2), d_nw_s0_dp_dq(1:50), d_nw_s0_dp_dq(51:100));
trimesh(fvidx, s0_marker(:,1), s0_marker(:,2));

s_dagger = s_target+ds;

new_q = s_star\(s_dagger-s0);

s_dagger_v = reshape(s_dagger, 50, 2);

s_v = ([1+new_q(1) new_q(2); -new_q(2) 1+new_q(1)] * (s_dagger_v - repmat(new_q(3:4)', 50, 1))' / ((1+new_q(1))^2 + new_q(2)^2))';

new_p = s'*(s_v(:)-s0);

new_pq = [new_p;new_q];


dpq = new_pq - pq;

new_pq = linesearch(T0, s0, s, srcimage, pq, dpq);

end



function [residue, Idiff, warpedImage] = getResidue(T0, s0, s, srcimage, pq)

    RS = size(pq,1)-4;

    p = pq(1:RS);
    q = pq(RS+1:RS+4);

    ws = s*p + s0;

    s_star = zeros(100,4);

    s_star(:,1) = s0(:);
    s_star(1:50,2) = -s0(51:100);
    s_star(51:100,2) = s0(1:50);
    s_star(1:50,3) = 1;
    s_star(51:100,4) = 1;

    s_target = ws + s_star*q;
    s_target_marker = getStandardfacemarker(s_target);

    s0_marker = getStandardfacemarker(s0);

    warpedImage = warpMarkings(srcimage, s_target_marker, s0_marker);

    [m,n,~] = size(warpedImage);

    Idiff = double(warpedImage) - double(reshape(T0, m, n, 3));
    residue = sum(Idiff(:).^2);

end

function pq_opt = linesearch(T0, s0, s, srcimage, pq, dpq)
global opt_global

opt_global.T0 = T0;
opt_global.s0 = s0;
opt_global.s = s;
opt_global.srcimage = srcimage;
opt_global.pq = pq;
opt_global.dpq = dpq;

a_opt = fminsearch(@goodness, 0.5,   optimset('Display','Iter','TolFun',1));
pq_opt = pq + a_opt*dpq;

end


function r = goodness(x)
global opt_global
    r = getResidue(opt_global.T0, opt_global.s0, opt_global.s, ...
        opt_global.srcimage, opt_global.pq+ x*opt_global.dpq);
end

