function destImage = warpMarkings(srcimage, srcMarkings, destMarkings)

[fvidx bgidx] = getFaceVertexIndices();

if size(destMarkings,1) == max(max(fvidx))
    [destlm desta destb] = getDestLookupMap(destMarkings, fvidx);
else
    [destlm desta destb] = getDestLookupMap(destMarkings, bgidx);
end

[m, n] = size(destlm);

destImage = double(zeros(m,n,3));

[ms, ns, ~] = size(srcimage);

for y = 1:m
    for x = 1:n
        T = destlm(y,x);
        
        if T~=0
            p = srcMarkings(bgidx(T,:), :);
            w = floor(p(1,:) + desta(y,x)*(p(2,:)-p(1,:)) + destb(y,x)*(p(3,:)-p(1,:)));
            destImage(y,x,:) = srcimage(max(min(w(2), ms), 1), max(min(w(1), ns), 1), :);
        end
    end
end


end


