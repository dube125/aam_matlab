function new_pqa = updateWarpAffinePQSimplex(T0, s0, s, srcimage, pqa)
global opt_global
opt_global.s0 = s0;
opt_global.s = s;
opt_global.srcimage = srcimage;
new_pqa = fminsearch(@(x)getResidue(T0, s0, s, srcimage, x), pqa, optimset('Display','iter','TolFun', 0.1,'OutputFcn',@outfun));

end



function [residue, Idiff, warpedImage] = getResidue(T0, s0, s, srcimage, pqa)

    RS = size(s,2);

    p = pqa(1:RS);
    q = pqa(RS+1:RS+4);
    a = pqa(RS+5:end);
    
    ws = s*p + s0;

    s_star = zeros(100,4);

    s_star(:,1) = s0(:);
    s_star(1:50,2) = -s0(51:100);
    s_star(51:100,2) = s0(1:50);
    s_star(1:50,3) = 1;
    s_star(51:100,4) = 1;

    s_target = ws + s_star*q;
    s_target_marker = getStandardfacemarker(s_target);

    s0_marker = getStandardfacemarker(s0);

    warpedImage = warpMarkings(srcimage, s_target_marker, s0_marker);
    
    warpedImage(:,:,1) = warpedImage(:,:,1)*(1+a(1)) + a(2);
    warpedImage(:,:,2) = warpedImage(:,:,2)*(1+a(3)) + a(4);
    warpedImage(:,:,3) = warpedImage(:,:,3)*(1+a(5)) + a(6);
    
    [m,n,~] = size(warpedImage);

    Idiff = double(warpedImage) - double(reshape(T0, m, n, 3));
    residue = sum(Idiff(:).^2);
    %tw = double(reshape(warpedImage, m* n* 3,1));
    %residue = (tw-mean(tw))'*(T0-mean(T0));
end


function stop = outfun(x, optimValues, state)
global opt_global
RS = size(x,1)-10;
target_s = getTranslate(opt_global.s0, opt_global.s(:,1:RS), x(1:RS), x(RS+1:RS+4));
target_markings = getStandardfacemarker(target_s);
hold off
showmarkings(opt_global.srcimage, target_markings);
hold on
trimesh(getFaceVertexIndices, target_markings(:,1), target_markings(:,2));
drawnow;
stop = false;

end