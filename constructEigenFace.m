function new_face = constructEigenFace(face_mean, U, u)    
    new_face = face_mean + U*u(:);
end