function faceExamine(foldername)

    load([foldername '/succimgsave.mat']);
    N = length(person_success_images);
    
    Nrows = ceil(N/6);
    figure;
    for i = 1:N
        subplot(Nrows, 6, i);
        
        img = imread(person_success_images(i).filename);
        r = person_success_images(i).faceinfo;
        [face_image, eye_info] = extractFaceImage(img, r, 128);
        imshow(face_image);
        
        
        if eye_info(1,3)/eye_info(2,3) > 1.1 || eye_info(1,3)/eye_info(2,3)<0.909
            eye_color = [1 0 0];
        else
            eye_color = [0 1 0];
        end
        
        rectangle('Position', eye_info(1,:), 'EdgeColor',eye_color);
        rectangle('Position', eye_info(2,:), 'EdgeColor',eye_color);
        
    end
end