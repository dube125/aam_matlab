function blendimage = blendbyMarkings(srcimage, srcMarkings, destimage, destMarkings, t)

[m0, n0, ~] = size(srcimage);

[m1, n1, ~] = size(destimage);


intro_markings = (1-t) * [srcMarkings(:,1)/m0*m1 srcMarkings(:,2)/n0*n1] + t * [destMarkings(:,1) destMarkings(:,2)];

[fvidx bgidx] = getFaceVertexIndices();

if size(destMarkings,1) == max(max(fvidx))
    [destlm desta destb] = getDestLookupMap(intro_markings, fvidx);
else
    [destlm desta destb] = getDestLookupMap(intro_markings, bgidx);
end

[m, n] = size(destlm);

blendimage = zeros(size(destimage));

for y = 1:m
    for x = 1:n
        T = destlm(y,x);
        if T~=0
            ps = srcMarkings(bgidx(T,:), :);
            ws = floor(ps(1,:) + desta(y,x)*(ps(2,:)-ps(1,:)) + destb(y,x)*(ps(3,:)-ps(1,:)));
            
            pd = destMarkings(bgidx(T,:), :);
            wd = floor(pd(1,:) + desta(y,x)*(pd(2,:)-pd(1,:)) + destb(y,x)*(pd(3,:)-pd(1,:)));
            
            if all([ws wd]>0)
                blendimage(y,x,:) = (1-t) * srcimage(ws(2), ws(1),:) + t * destimage(wd(2),wd(1),:);
            end
        end
    end
end


end


