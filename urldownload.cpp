#include <stdio.h>
#include <iostream>
#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include <string>

#include "mex.h"


size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written;
    written = fwrite(ptr, size, nmemb, stream);
    return written;
}


void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
   
    
    
    if (nrhs == 2) {
        char *url = mxArrayToString(prhs[0]);
        char *outfilename = mxArrayToString(prhs[1]);
        
        CURL *curl;
        FILE *fp;
        CURLcode res;
 
        
        curl = curl_easy_init();
        
        
        if(curl)
        {
            fp = fopen(outfilename,"wb");
            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
            curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5);

            //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

            //curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
            res = curl_easy_perform(curl);
            if (nlhs == 1) {
                plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL); 
                double * yp = mxGetPr(plhs[0]);
                *yp = res;
            }
            curl_easy_cleanup(curl);
            fclose(fp);
        }        
        
        mxFree(url);
        mxFree(outfilename);
    } else {
        std::cout<<"Usage: urldownload(url, savefile)"<<std::endl;
    }
	return;
}