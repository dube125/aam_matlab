function showmarkings(varargin)
% SHOWMARKINGS - show face markings
% Usage:
%   showmarkings(faceimg, markings) 
%   showmarkings(filename)

    if nargin == 2
        faceimg = varargin{1};
        markings = varargin{2};
    else
        load(varargin{1});
        faceimg = facemarking.faceimg;
        markings = facemarking.markings;
    end
    
    hold off
    imshow(faceimg);
    hold on
    plot(markings(:,1), markings(:,2), 'b.');
    for i = 1:size(markings,1)
        text(markings(i,1), markings(i,2)-5, num2str(i));
    end
    
end