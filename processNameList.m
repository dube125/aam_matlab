function [ str_collect ] = processNameList( filename )
%PROCESSNAMELIST Summary of this function goes here
%   Detailed explanation goes here
fid = fopen('namelist.txt');
b = fread(fid, '*uint8');
fclose(fid);
N = 1;
str_collect = {};

s = '';

idx = 4;
while idx < length(b)
  while sum(b(idx:idx+1) ~= [13; 10])
      s = [s '%' dec2hex(b(idx))];
      idx = idx + 1;
  end  
  idx = idx + 2;
  str_collect = {str_collect{:}, s};
  s = '';
end

end

