function [face_collect meta_collect] = collectAllFaces(foldername, idx)
    
    C = makecform('srgb2lab');
    
    face_collect = zeros(141*128,0);
    meta_collect = cell(0,0);
    
    face_count = 1;
    for i=idx
        cname = [foldername '/c' num2str(i, '%04d')];
        load([cname '/succimgsave.mat']);
        N = length(person_success_images);
        
        for j = 1:N
            img = imread(person_success_images(j).filename);
            r = person_success_images(j).faceinfo;
            [face_image, eye_info] = extractFaceImage(img, r, 128);
            
            if eye_info(1,3)/eye_info(2,3) < 1.1 && eye_info(1,3)/eye_info(2,3)>0.909
                
                [m,n,~] = size(face_image);
                
                if all([m,n] == [141 128])
                    img_lab = applycform(face_image,C);

                    face_collect(:,face_count) = reshape(img_lab(1:141,1:128,1), 141*128, 1);
                    person_success_images(j).eyeinfo = eye_info;
                    person_success_images(j).personid = i;
                    
                    meta_collect{face_count} = person_success_images(j);

                    face_count = face_count +1;
                end
            
            end

       
        end    
    end
end