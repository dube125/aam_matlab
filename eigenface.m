function [U, S, face_mean, face_remain] = eigenface(face_collect, R)

    fc_max = max(face_collect);
    fc_min = min(face_collect);
    
    n = length(face_collect);
    
    face_collect_norm = (face_collect - repmat(fc_min, n, 1))./repmat(fc_max-fc_min, n, 1);
    
    face_mean = mean(face_collect_norm,2);
    
    face_remain = face_collect_norm - repmat(face_mean, [1, size(face_collect_norm,2)]);
    
%     L = cov(face_remain);
%     [V, D] = eig(L);
%     
%     [D_sorted, D_idx] = sort(diag(D), 'descend');
%     
%     figure,semilogx(D_sorted, '-*');
%     ylabel('Eigenvalue magnitude');
%     xlabel('Eigenvalue index');
%     
%     V_sorted = V(:,D_idx(1:R));
%     U = normc(face_remain*V_sorted);
%   
    [U_all, S_all, ~] = svd(face_remain,'econ');
    S = S_all(1:R, 1:R);
    U = U_all(:,1:R);
end