function u = getEigenFaceVector(U, face_mean, face)

    face_norm = (face-min(face))/(max(face)-min(face));
    u = U'*(face_norm - face_mean);
end