function [save_file_names download_success] = downloadImageEntries(image_entries, filename_prefix, save_path, start_idx)

if ~isdir(save_path)
    mkdir(save_path);
end

save_file_names = cell(length(image_entries), 1);
download_success = zeros(length(image_entries), 1);

if isdir(save_path)
    
    for i = 1:length(image_entries)
        fname = [save_path '/' filename_prefix '_p' num2str(i+start_idx-1, '%04d') '.jpg'];
        try
            %urlwriteLS(image_entries(i).url, fname);
            system(['curl -s -m 10 -o', fname, ' ', image_entries(i).url]);
            save_file_names{i} = fname;
            file_test = dir(fname);
            if length(file_test)>0 && file_test(1).bytes>0
                download_success(i) = 1;
                fprintf('  %s <- %s\n', fname, image_entries(i).url);
            else
                fprintf('error downloading %s\n', image_entries(i).url);
            end
        catch err
            fprintf('error downloading %s\n', image_entries(i).url);
        end
    end
    

end