function image_entries = extractGoogleImageURLs(html_str) 

[image_items] = regexp(html_str, '<td style="width:25%;word-wrap:break-word">.*?</td>', 'match');
N = length(image_items);

image_url = cell(N,1);
image_refurl = cell(N,1);
image_site = cell(N,1);



for i = 1:length(image_items)
    url = regexp(image_items{i}, 'imgurl.*?\.jpg', 'match');
    
    if ~isempty(url)
        image_url{i} = url{1}(8:end); 
    end
    
    
    refurl = regexp(image_items{i}, 'imgrefurl.*?&amp', 'match');
    if ~isempty(refurl)
        image_refurl{i} = refurl{1}(11:end-4); 
    end
    
    site = regexp(image_items{i}, '<cite title=".*?">', 'match');
    if ~isempty(site)
        image_site{i} = site{1}(14:end-2); 
    end
    
end

image_entries = struct('url', image_url, 'refurl', image_refurl, 'site', image_site);

end