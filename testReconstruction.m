%%
opencvtest(meta_collect(136).filename)

%%
R = 100;
[U, S, face_mean] = eigenface(face_collect, R);

u = U'*(face_remain);

%%
idx = 253;


figure(1)
imshow(meta_collect(idx).filename);
new_face = constructEigenFace(face_mean, U, u(:,idx));
figure(2)
imshow(reshape(new_face, 141, 128), [0 1]);
figure(3)
imshow(reshape(face_collect_norm(:,idx), 141, 128));


%%

n = 40;
colors = hsv(meta_collect(n).personid);

figure(4)
hold on
grid on
for i=1:n
    plot3(u(5,i), u(6,i), u(7,i), 'o', 'MarkerFaceColor', colors(meta_collect(i).personid, :));
end


%%
idx1 = 178;

dist = u(5:end,idx1)'*inv(S(5:end, 5:end)*S(5:end, 5:end))*u(5:end,:);

[dist_sorted, dist_idx] = sort(dist,'descend');

plot(dist_sorted, '-*');




%% face mixing

idx2 = dist_idx(3);

figure(2)

subplot(2,1,1);
imshow(reshape(face_collect_norm(:,idx1), 141, 128));
subplot(2,1,2);
imshow(reshape(face_collect_norm(:,idx2), 141, 128));

RN = 7;
r = linspace(0,1,RN);
figure(2)
for i = 1:RN
    subplot(1, RN, i);
    new_face = constructEigenFace(face_mean, U, r(i)*u(:,idx1)+(1-r(i))*u(:,idx2));
    imshow(reshape(new_face, 141, 128), [0 1]);
end
