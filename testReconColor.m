%%
R = 80;
[U, S, face_mean, face_remain] = eigenface(face_collect_color, R);
u = U'*(face_remain);

%%
idx = 223;


figure(1)
imshow(meta_collect(idx).filename);
new_face = constructEigenFace(face_mean, U, u(:,idx));
figure(2)
imshow(reshape(new_face, 141, 128, 3), [0 1]);
figure(3)
imshow(reshape(face_mean, 141, 128, 3));


%%

n = 40;
colors = hsv(meta_collect(n).personid);
cut_off = 12;
figure(4)
hold on
grid on
for i=1:n
    plot3(u(cut_off,i), u(cut_off+1,i), u(cut_off+2,i), 'o', 'MarkerFaceColor', colors(meta_collect(i).personid, :));
end


%%
idx1 = 45;

dist = u(5:end,idx1)'*inv(S(5:end, 5:end)*S(5:end, 5:end))*u(5:end,:);

[dist_sorted, dist_idx] = sort(dist,'descend');

figure(2);
subplot(2,1,1);
imshow(reshape(face_remain(:,idx1) + face_mean, 141, 128, 3));
subplot(2,1,2);
plot(dist_sorted, '-*');




%% face mixing

idx2 = dist_idx(3);

figure(2)

subplot(2,1,1);
imshow(reshape(face_remain(:,idx1) + face_mean, 141, 128, 3));
subplot(2,1,2);
imshow(reshape(face_remain(:,idx2) + face_mean, 141, 128, 3));

RN = 7;
r = linspace(0,1,RN);
figure(3)
for i = 1:RN
    subplot(1, RN, i);
    new_face = constructEigenFace(face_mean, U, r(i)*u(:,idx1)+(1-r(i))*u(:,idx2));
    imshow(reshape(new_face, 141, 128, 3), [0 1]);
end


%% face mixing

idx3 = ceil(rand(1)*438);
figure(2)

subplot(3,1,1);
imshow(reshape(face_remain(:,idx1) + face_mean, 141, 128, 3));
subplot(3,1,2);
imshow(reshape(face_remain(:,idx2) + face_mean, 141, 128, 3));
subplot(3,1,3);
imshow(reshape(face_remain(:,idx3) + face_mean, 141, 128, 3));

RN = 7;
r = linspace(0,1,RN);
figure(3)
for i = 1:RN
    for j = 1:i
        r2 = linspace(0,1,i);
        subplot(RN, RN, (i-1)*RN + j);
        new_face = constructEigenFace(face_mean, U, r(end+1-i) * u(:,idx3) + (1-r(end+1-i)) *(r2(j)*u(:,idx1)+(1-r2(j))*u(:,idx2)));
        imshow(reshape(new_face, 141, 128, 3), [0 1]);
    end
end

%%
figure(4)
displayImageColorSpace(uint8(reshape(face_remain(:,ceil(rand(1)*438)) + face_mean, 141, 128, 3)*256))
