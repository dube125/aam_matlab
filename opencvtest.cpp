////////////////////////////////////////////////////////////////////////
//
//showImageMt.cpp
//
// This is a simple, introductory OpenCV program. The program reads an
// image from a file, inverts it, and displays the result.
//
////////////////////////////////////////////////////////////////////////
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

#include "mex.h"

using namespace std;
using namespace cv;

 /** Function Headers */
void detectAndDisplay(int nlhs, mxArray* plhs[], Mat frame );

 /** Global variables */
char * face_cascade_name = "haarcascade_frontalface_alt.xml";
char * eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
char * mouth_cascade_name = "haarcascade_mcs_mouth.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
CascadeClassifier mouth_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);

 /** @function main */
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]){
  
  Mat img;
  if (nrhs == 1) {
    char *name = mxArrayToString(prhs[0]);
    img = imread(name, CV_LOAD_IMAGE_COLOR);
    mxFree(name);
  } else {
    img = imread("test.jpg", CV_LOAD_IMAGE_COLOR);
    
  } 
  
   //-- 1. Load the cascades
  if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return; };
  if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return; };
  if( !mouth_cascade.load( mouth_cascade_name ) ){ printf("--(!)Error loading mouth detector\n"); return; };

  detectAndDisplay(nlhs, plhs, img );
  //imshow(window_name,img);
  cvWaitKey(500);
  cvDestroyWindow(window_name.c_str());
  return;
}

// /** @function detectAndDisplay */
void detectAndDisplay(int nlhs, mxArray* plhs[], Mat frame )
{
  std::vector<Rect> faces;
  Mat frame_gray;

  cvtColor( frame, frame_gray, CV_BGR2GRAY );
  equalizeHist( frame_gray, frame_gray );

  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
  int total_eyes = 0;
  std::vector< vector<Rect> > eyes_collect;
  
  int total_mouth = 0;
  std::vector< vector<Rect> > mouth_collect;
  std::vector<int> detected_mouth_collect;
  
  
  for( int i = 0; i < faces.size(); i++ )
  {
    rectangle(frame, faces[i], Scalar( 255, 0, 255 ));
    Mat faceROI = frame_gray( faces[i] );
    std::vector<Rect> eyes;

    //-- In each face, detect eyes
    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(4, 4) );

    for( int j = 0; j < eyes.size(); j++ )
    {
        Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
        int radius = cvRound( (eyes[j].width + eyes[i].height)*0.25 );
        circle( frame, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
        ++total_eyes;
    }
    
    eyes_collect.push_back(eyes);
    
    std::vector<Rect> mouth;
    
    //-- In each face, detect mouth
    mouth_cascade.detectMultiScale( faceROI, mouth, 1.1, 3, 0 |CV_HAAR_SCALE_IMAGE, Size(10, 10) );
    int detected_mouth = -1;
    for( int j = 0; j < mouth.size(); j++ )
    {
        if (detected_mouth == -1 ||((mouth[j].y+ mouth[j].height/2)>(mouth[detected_mouth].y+ mouth[detected_mouth].height/2)) && 
                (eyes.size()>0) && (mouth[j].width>eyes[0].width)) {
            detected_mouth = j;
        }
    }
    if (detected_mouth != -1) {
        rectangle(frame, Rect(faces[i].x + mouth[detected_mouth].x, faces[i].y + mouth[detected_mouth].y, 
                mouth[detected_mouth].width, mouth[detected_mouth].height), Scalar(0, 128, 255)); 
        ++total_mouth;
    }
    mouth_collect.push_back(mouth);
    detected_mouth_collect.push_back(detected_mouth);
  }
  
//   std::cout<<"Total faces detected: "<<faces.size()<<endl;
//   std::cout<<"Total eyes detected: "<<total_eyes<<endl;

  int m = faces.size()+total_eyes+total_mouth;
  
  if (nlhs == 1) {
    plhs[0] = mxCreateDoubleMatrix(m, 5, mxREAL); 
    double * yp = mxGetPr(plhs[0]);
    int idx = 0;
    for (int i = 0; i < faces.size(); i++) {
        
        yp[idx   ] = faces[i].x;
        yp[idx + 1 * m] = faces[i].y;
        yp[idx + 2 * m] = faces[i].width;
        yp[idx + 3 * m] = faces[i].height;
        yp[idx + 4 * m] = 0;
        ++idx;
        
        for (int j = 0; j < eyes_collect[i].size(); j++) {
            yp[idx   ] = eyes_collect[i][j].x;
            yp[idx + 1 * m] = eyes_collect[i][j].y;
            yp[idx + 2 * m] = eyes_collect[i][j].width;
            yp[idx + 3 * m] = eyes_collect[i][j].height;
            yp[idx + 4 * m] = i+1;
            ++idx;
        } 
        
        if (detected_mouth_collect[i] != -1) {
            yp[idx   ] = mouth_collect[i][detected_mouth_collect[i]].x;
            yp[idx + 1 * m] = mouth_collect[i][detected_mouth_collect[i]].y;
            yp[idx + 2 * m] = mouth_collect[i][detected_mouth_collect[i]].width;
            yp[idx + 3 * m] = mouth_collect[i][detected_mouth_collect[i]].height;
            yp[idx + 4 * m] = -(i+1);
            ++idx;        
        }
    }
  }
    
    /* Assign pointers to the various parameters */ 
  

  //-- Show what you got
  imshow( window_name, frame );
 }
