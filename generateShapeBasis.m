function [s0, s] = generateShapeBasis(S)
% generate normalized average shape and shape basis matrix s

% initial estimation
avgmarker = zeros(50,2);
Nmarkers = size(S, 2);
Sp = zeros(100, Nmarkers);

% preliminary normalization to [-0.5 0.5; -0.5 0.5]

for i = 1:Nmarkers
    marker = reshape(S(:,i), 50, 2);
    markerspan = 2*max(abs([min(marker);max(marker)] - repmat(mean(marker), 2,1)));
    marker_normalized = (marker - repmat( mean(marker), 50, 1) )./repmat( markerspan, 50, 1);
    Sp(:,i) = marker_normalized(:);
    
    avgmarker = avgmarker + marker_normalized;
end

s0 = avgmarker(:)/Nmarkers;


% refinement normalization with rotation, scale and translation
for k = 1:3
    
    Spp = zeros(100, Nmarkers);
    s0p = zeros(100,1);

    for i = 1:Nmarkers
        s_star = zeros(100,4);
        s_star(:,1) = Sp(:,i);
        s_star(1:50,2) = -Sp(51:100,i);
        s_star(51:100,2) = Sp(1:50,i);
        s_star(1:50,3) = 1;
        s_star(51:100,4) = 1;
        
        %q = s0\s_star;
        q = fminsearch(@(x) sum((s_star*x'-s0).^2), [0,0,0,0]);
        Spp(:,i) = s_star*q';
        s0p = s0p+Spp(:,i);
    end
    s0p = s0p/Nmarkers;
    s0 = s0p;
    Sp = Spp;
end


[s sig sv] = svd(Sp-repmat(s0(:), 1, Nmarkers) ,0);

figure
plot(diag(sig),'-*');
xlabel('Singular Value Index');
ylabel('Singular Value');
title('Shape Matrix Singular Values');

