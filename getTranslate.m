function s_new = getTranslate(s0, s, p, q)
s1 = s * p + s0;
% 
% s_star = zeros(100,4);
% 
% s_star(:,1) = s0(:);
% s_star(1:50,2) = -s0(51:100);
% s_star(51:100,2) = s0(1:50);
% s_star(1:50,3) = 1;
% s_star(51:100,4) = 1;
% 
% s_new = s1 + s_star*q;


R = [cos(q(2)), sin(q(2)); -sin(q(2)) cos(q(2))];
s_new = (1+q(1))*reshape(reshape(s1,50,2)*R', 100,1) + [ones(50,1); zeros(50,1)]*q(3) + [zeros(50,1); ones(50,1)]*q(4);

end

