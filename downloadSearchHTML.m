function html_str = downloadGoogleSearchHTML(url_prefix, url_suffix, search_term)
    html_str = urlread([url_prefix search_term url_suffix]);
end